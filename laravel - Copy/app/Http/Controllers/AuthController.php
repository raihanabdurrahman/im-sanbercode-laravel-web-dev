<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg()
    {
        return view('register');
    }
    public function wel(Request $request)
    {
        $fnama = $request['fname'];
        $lnama = $request['lname'];
        return view('welcome',['fnama' => $fnama,'lnama' => $lnama]);
    }


}
