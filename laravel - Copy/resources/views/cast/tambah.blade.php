@extends('layout.master')

@section('judul')
    Halaman Tambah Cast  
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="name" class="form-control @error('name') is-invalid   
      @enderror" >
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" class="form-control @error('umur') is-invalid   
      @enderror" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Bio</label>
      <input type="text" name="bio" class="form-control @error('bio') is-invalid   
      @enderror">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection