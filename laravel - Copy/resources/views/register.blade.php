<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <!-- Header -->
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <!-- First Name & Last Name -->
    <form action="/welcome" method="post">
        @csrf
      <label> First name: </label><br /><br />
      <input type="text" name="fname" /><br /><br />
      <label> Last name: </label><br /><br />
      <input type="text" name="lname" /><br /><br />

      <!-- Gender -->
      <label> Gender: </label><br /><br />
      <input type="radio" value="1" name="status" />Male<br />
      <input type="radio" value="2" name="status" />Female<br />
      <input type="radio" value="3" name="status" />Other<br /><br />

      <!-- Nationality -->
      <label> Nationality: </label><br /><br />
      <select name="Country">
        <option value="1">Indonesia</option>
        <option value="2">Singapore</option>
        <option value="3">Malaysia</option>
      </select>
      <br /><br />

      <!-- Languange Spoken -->
      <label> Language Spoken: </label><br /><br />
      <input type="checkbox" value="1" name="Language" />Bahasa Indonesia <br />
      <input type="checkbox" value="2" name="Language" />English <br />
      <input type="checkbox" value="3" name="Language" />Other <br /><br />

      <!-- Bio -->
      <label> Bio: </label><br /><br />
      <textarea name="message" cols="35" rows="10"></textarea>
      <br />

      <!-- Submit Button -->
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
