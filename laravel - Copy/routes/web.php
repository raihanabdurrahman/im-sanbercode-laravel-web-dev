<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'utama']);

Route::get('/register',[AuthController::class,'reg']);

Route::post('/welcome',[AuthController::class,'wel']);

Route::get('/table',function(){
    return view('halaman.table');
});
Route::get('/data-table',function(){
    return view('halaman.dtable');
});


//CRUD

//CREATE DATA
//route form
Route::get('/cast/create',[	CastController::class, 'create']);
//route insert data ke database
Route::post('/cast',[CastController::class, 'store']);

//READ DATA
//Route Meampilkan semua data pada table cast
Route::get('cast',[CastController::class, 'index']);
Route::get('cast/{id}',[CastController::class, 'show']);

//UPdate Data
//Route Edit data
Route::get('/cast/{id}/edit',[CastController::class, 'edit']);
Route::put('/cast/{id}',[CastController::class, 'update']);

//Delete data
Route::delete('/cast/{id}',[CastController::class, 'destroy']);