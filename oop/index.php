<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("shaun");
echo "Name: " . $sheep->getName() . "<br>"; 
echo "Legs: " . $sheep->getLegs() . "<br>"; 
echo "Cold blooded: " . $sheep->getColdBlooded() . "<br><br>"; 

$kodok = new Frog("buduk");
echo "name: " . $kodok->getName() . "<br>"; // "buduk"
echo "legs: " . $kodok->getLegs() . "<br>"; // 4
echo "cold blooded: " . $kodok->getColdBlooded() . "<br>"; // "no"
echo $kodok->jump() . "<br><br>"; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "name: " . $sungokong->getName() . "<br>"; // "kera sakti"
echo "legs: " . $sungokong->getLegs() . "<br>"; // 2
echo "cold blooded: " . $sungokong->getColdBlooded() . "<br>"; // "no"
$sungokong->yell(); // "Auooo"